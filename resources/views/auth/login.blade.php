@extends('layouts.auth')
@section('title',__("Se conncecter"))
@section('content')
<!DOCTYPE html>
<html lang="en">
  
<!-- Mirrored from themepixels.me/slim1.1/template/page-signin.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 27 May 2020 12:43:29 GMT -->
<head>
   
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>Slim Responsive Bootstrap 4 Admin Template</title>

    <!-- Vendor css -->
    <link href="../lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="../lib/Ionicons/css/ionicons.css" rel="stylesheet">

    <!-- Slim CSS -->
    <link rel="stylesheet" href="../css/slim.css">

  </head>
  <body>

  <div class="signin-wrapper card-body">
    <form method="POST" action="{{ route('login') }}">
    @csrf
      <div class="signin-box">
        <h2 class="slim-logo"><a href="{{url('/')}}">MoodleR<span>.</span></a></h2>
        <h2 class="signin-title-primary">{{ __('Bienvenue') }}</h2>
        <h4 class="signin-title-secondary"> {{ __('Connectez-vous pour continuer.') }}</h4>
        
        <div class="form-group">
                <input id="email" placeholder="{{__('Entrez votre adresse email')}}" type="email" class="form-control @error('password') is-invalid @enderror" value="{{old('email')}}" name="email" required autocomplete="current-email">
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
        </div>
        <div class="form-group ">
                <input id="password" type="password" placeholder='{{__("Entrez votre mot de passe")}}' class="form-control @error('password') is-invalid @enderror" name="password" required >

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
        </div>
        <div class="form-group mg-b-50 row text-sm">
            <div class="col-md-6">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                    <label class="form-check-label" for="remember">
                        {{ __('Rester connecté') }}
                    </label>
                </div>
            </div>
            
            <div class="col-md-6 text-right">
            @if (Route::has('password.request'))
             <a class="" href="{{ route('password.request') }}">
                    {{ __('Mot de passe oublié ?') }}
                </a>
            @endif    
        </div>
        </div>
        <button class="btn btn-primary btn-block btn-signin"> {{ __('Se connecter') }}</button>
        <p class="mg-b-0 text-center"> {{ __('Nouveau,') }}<a href="{{url('register')}}"> {{ __('Créer un compte') }}</a></p>
      </div><!-- signin-box -->
    </div><!-- signin-wrapper -->
    </form>

    <script src="../lib/jquery/js/jquery.js"></script>
    <script src="../lib/popper.js/js/popper.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.js"></script>

    <script src="../js/slim.js"></script>

  </body>

<!-- Mirrored from themepixels.me/slim1.1/template/page-signin.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 27 May 2020 12:43:29 GMT -->
</html>
@endsection
