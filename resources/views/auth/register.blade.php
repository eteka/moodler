@extends('layouts.auth')

@section('title',__("Créer un  compte"))

@section('content')

    <div class="signin-wrapper card-body">
    <form method="POST" action="{{ route('register') }}">
                        @csrf
        <div class="signin-box">
        <h2 class="slim-logo"><a href="{{url('/')}}">MoodleR<span>.</span></a></h2>
        <h3 class="signin-title-primary">{{__('Démarrons !')}}</h3>
        <h5 class="signin-title-secondary lh-4">{{__("L'inscription prend environ une minute.")}}</h5>

        <div class="row row-xs mg-b-10">
        <div class="col-sm-6 mg-t-10 mg-sm-t-0">
        <input id="nom" type="text"  placeholder="{{__('Nom de famille')}}" class="form-control @error('nom') is-invalid @enderror" name="nom" value="{{ old('nom') }}" required autocomplete="nom" autofocus>

                @error('nom')
                    <span class="invalid-feedback col-sm-12" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
        </div>
        <div class="col-sm-6 mg-t-10 mg-sm-t-0">
        <input id="prenom" type="text"  placeholder="{{__('Prénom')}}" class="form-control @error('prenom') is-invalid @enderror" name="prenom" value="{{ old('prenom') }}" required autocomplete="prenom" >

                @error('prenom')
                    <span class="invalid-feedback col-sm-12" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
        </div>
        </div>
        <div class="row row-xs mg-b-10">
            <div class="col-sm-12 mg-t-10 mg-sm-t-0">
            <input id="email"  type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" class="form-control" placeholder="{{__('Courrier électronique')}}">
            
            @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
        </div>
        </div>

        <div class="row row-xs mg-b-10">
            <div class="col-sm">
                <input id="password" type="password" placeholder="{{__('Mot de passe')}}" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

               
            </div>
            <div class="col-sm mg-t-10 mg-sm-t-0">
            <input id="password-confirm" type="password" placeholder="{{__('Confirmation du mot depasse')}}" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                
        </div>
        <div class="col-md-12">
              @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
              @enderror
        </div>
        </div><!-- row -->

        <button class="btn btn-primary btn-block btn-signin mt-4">{{__('Créer un compte')}}</button>
       <hr>
        <!--div class="signup-separator"><span>ou se connecter</span></div>

        <button class="btn btn-facebook btn-block">Sign Up Using Facebook</button>
        <button class="btn btn-twitter btn-block">Sign Up Using Twitter</button-->

        <p class="mg-t-40 mg-b-0">{{__('Avez-vous déjà un compte ?')}} <a href="{{url('login')}}">{{__('Connectez-vous')}}</a></p>
        </div>
        <!-- signin-box -->
    </form>
</div>
<!--div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div-->
@endsection
