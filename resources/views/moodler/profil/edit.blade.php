@extends('layouts.app')

@section('title',__("Edition de profil :"). Auth::user()->fullname())

@section('content')

<div class="main ">
<div class="card-column row mg-t-20">
           <div class="col-md-4">
              <div class="card card-body pd-25">
                <h6 class="slim-card-title">Get Connected</h6>
                <p>Just select any of your available social account to get started.</p>
                <div class="tx-20">
                  <a href="#" class="tx-primary mg-r-5"><i class="fa fa-facebook"></i></a>
                  <a href="#" class="tx-info mg-r-5"><i class="fa fa-twitter"></i></a>
                  <a href="#" class="tx-danger mg-r-5"><i class="fa fa-google-plus"></i></a>
                  <a href="#" class="tx-danger mg-r-5"><i class="fa fa-pinterest"></i></a>
                  <a href="#" class="tx-inverse mg-r-5"><i class="fa fa-github"></i></a>
                  <a href="#" class="tx-pink mg-r-5"><i class="fa fa-instagram"></i></a>
                </div>
              </div><!-- card -->
           </div>
          
          <div class="col-md-8">
          <div class="card">
            <div class="card-body pd-30">
              <h6 class="slim-card-title">Payment Information</h6>
              <div class="form-group mg-t-20">
                <label class="form-control-label mg-r-10">Pay using your:</label>
                <label class="rdiobox d-inline-block">
                  <input name="radio" type="radio" checked="">
                  <span>Credit/Debit Card</span>
                </label>
                <label class="rdiobox d-inline-block mg-xs-l-20">
                  <input name="radio" type="radio">
                  <span>Paypal</span>
                </label>
              </div><!-- form-group -->

              <p class="tx-32">
                <i class="fa fa-cc-visa mg-r-5"></i>
                <i class="fa fa-cc-mastercard mg-r-5"></i>
                <i class="fa fa-cc-discover"></i>
              </p>

              <div class="form-group">
                <label class="form-control-label">Card Number</label>
                <input type="text" name="cardno" class="form-control">
              </div><!-- form-group -->

              <div class="form-group">
                <label class="form-control-label">Name on Card</label>
                <input type="text" name="cardname" class="form-control">
              </div><!-- form-group -->

              <div class="row">
                <div class="col-sm-5">
                  <div class="form-group">
                    <label class="form-control-label">Card Expiry</label>
                    <div class="row">
                      <div class="col">
                        <input type="text" name="cardmonth" placeholder="MM" class="form-control">
                      </div>
                      <div class="col">
                        <input type="text" name="cardyear" placeholder="YY" class="form-control">
                      </div>
                    </div>
                  </div><!-- form-group -->
                </div><!-- col-5 -->
                <div class="col-sm-4">
                  <div class="form-group">
                    <label class="form-control-label">CVV </label>
                    <input type="text" name="cardno" class="form-control">
                  </div><!-- form-group -->
                </div><!-- col-4 -->
              </div><!-- row -->

              <p class="mg-t-25">
                <label class="ckbox">
                  <input type="checkbox" checked="">
                  <span>I have read and accept the terms of use on this website.</span>
                </label>
              </p>

              <div class="mg-t-30">
                <button class="btn btn-primary pd-x-20">Submit</button>
                <button class="btn btn-secondary pd-x-20">Cancel</button>
              </div>
            </div><!-- card-body -->
          </div>
          </div>
          
        </div>
</div>
@endsection