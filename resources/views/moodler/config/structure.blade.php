@extends('layouts.app')

@section('title',"Configuration")

@section('content')
<div class="main">
    <div class="slim-pageheader">
          <ol class="breadcrumb slim-breadcrumb">
           
          </ol>
          <h6 class="slim-pagetitle">
            Configuration
          </h6>
    </div>
    
    <div class="section-wrapper mg-t-20 col-sm-9">
          <label class="section-title">{{__('Renseignement sur la structure')}}</label>
          <p class="mg-b-20 mg-sm-b-40">{{__("Veuillez remplir les champs ci-desous qui donnent plus de détails sur la structure pour laquelle l'application est utilisée")}}</p>

          <div id="wizard6"  role="application" class="wizard wizard-style-2  clearfix">
              <div class="steps clearfix"><ul role="tablist">
                  <li role="tab" class="first current" aria-disabled="false" aria-selected="true">
                      <a id="wizard6-t-0" href="{{route('configBdd')}}" aria-controls="wizard6-p-0">
                          <span class="number">1</span> <span class="title">Information sur la structure</span></a></li>
                          <li role="tab" class="disabled" aria-disabled="true">
                              <a id="wizard6-t-1" href="{{route('configBdd')}}" aria-controls="wizard6-p-1">
                                  <span class="number">2</span> 
                                  <span class="title">La base de données </span></a></li>
                                  <li role="tab" class="disabled last" aria-disabled="true">
                                      <a id="wizard6-t-2" href="#wizard6-h-2" aria-controls="wizard6-p-2">
                                          <span class="number">3</span> <span class="title">Préférences</span></a></li></ul></div>
                         
                          <div class="content clearfix">
                              
                                
                    <section id="wizard6-p-0" role="tabpanel" aria-labelledby="wizard6-h-0" class="body current" aria-hidden="false">
                    <!--form action="{{route('saveStructure')}}" method="POST" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data"-->
                    {!! Form::model($config, ['method' => 'POST', 'route' => 'saveStructure']) !!}
                    
                         @csrf
                    <div class="form-layout form-layout-5">
                        <div class="row">
                        <div class="col-md-12">
                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                        </div>
                        <label class="col-sm-3 form-control-label"><span class="tx-danger">*</span> {{__('Nom de la structure')}}:</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                        {{ Form::text('nom',null,['class'=>"form-control","placeholder"=>__('Université du Bénin')]) }}
                           
                        </div>
                        </div><!-- row -->
                        <div class="row mg-t-20">
                            <label class="col-sm-3 form-control-label">
                                {{__('Image du logo')}} :
                            </label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            {{ Form::file('logo',['class'=>"form-control","accept"=>".png,.jpg,.jpeg"]) }}
                       
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-3 form-control-label">
                                {{__('Localisation')}} :
                            </label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            {{ Form::text('localisation',null,['class'=>"form-control","placeholder"=>__('Bénin, Porto-Novo')]) }}                       
                            </div>
                        </div>
                        <div class="row mg-t-20">
                        <label class="col-sm-3 form-control-label">
                             {{__('Téléphone')}} :
                        </label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            {{ Form::text('tel',null,['class'=>"form-control","placeholder"=>__('+229 96 00 00 00')]) }}
                        </div>
                        </div>
                        <div class="row mg-t-20">
                        <label class="col-sm-3 form-control-label"> {{__('Email')}}:</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            {{ Form::email('email',null,['class'=>"form-control","placeholder"=>__("Entrer l'adresse email")]) }}
                        </div>
                        </div>
                        <div class="row mg-t-20">
                        <label class="col-sm-3 form-control-label v-top">
                             {{__('Description')}}:</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                        {{ Form::textarea('description',null,['class'=>"form-control","placeholder"=>__("Entrer la description"),"rows"=>2]) }}
                        
                        </div>
                        </div><!-- row -->
                        <div class="row mg-t-30">
                        <div class="col-sm-3">
                        </div>
                        <div class="col-sm-8 ">
                            <div class="form-layout-footer">
                            <button class="btn btn-primary bd-0" type="submit"><i class="fa fa-save"></i>   {{__('Sauvegarde')}}</button>
                            <a href="{{route('welcome')}}" class="btn btn-secondary bd-0"> <i class="fa fa-chevron-left"></i> {{__('Retour')}}</a>
                            </div><!-- form-layout-footer -->
                        </div><!-- col-8 -->
                        </div>
                    </div>
</form>
            </section>
          </div>
     </div>
    </div>
</div>
@endsection