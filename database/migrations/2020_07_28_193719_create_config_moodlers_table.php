<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConfigMoodlersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_moodlers', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('nom_structure')->nullable();
            $table->string('logo_tsructure')->nullable();
            $table->string('tel_tsructure')->nullable();
            $table->string('email_tructure')->nullable();
            $table->text('description_structure')->nullable();
            $table->string('localisation_structure')->nullable();
            $table->string('server_bdd')->nullable();
            $table->string('nom_bdd')->nullable();
            $table->string('user_bdd')->nullable();
            $table->string('pwd_bdd')->nullable();
            $table->integer('port_bdd')->nullable();
            $table->string('lang_default')->nullable();
            $table->string('fichier_default')->nullable();
            $table->string('format_default')->nullable();
            $table->string('type_secure')->nullable();
            $table->string('pays')->nullable();
            $table->dateTime('timezone')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('config_moodlers');
    }
}
