<?php

namespace App\Http\Controllers\moodler;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\config_structure;
use Illuminate\Http\Request;

class config_structureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $config_structure = config_structure::where('nom', 'LIKE', "%$keyword%")
                ->orWhere('logo', 'LIKE', "%$keyword%")
                ->orWhere('telephone', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('localisation', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $config_structure = config_structure::latest()->paginate($perPage);
        }

        return view('moodler.config_structure.index', compact('config_structure'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('moodler.config_structure.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        config_structure::create($requestData);

        return redirect('moodler/config_structure')->with('flash_message', 'config_structure added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $config_structure = config_structure::findOrFail($id);

        return view('moodler.config_structure.show', compact('config_structure'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $config_structure = config_structure::findOrFail($id);

        return view('moodler.config_structure.edit', compact('config_structure'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $config_structure = config_structure::findOrFail($id);
        $config_structure->update($requestData);

        return redirect('moodler/config_structure')->with('flash_message', 'config_structure updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        config_structure::destroy($id);

        return redirect('moodler/config_structure')->with('flash_message', 'config_structure deleted!');
    }
}
