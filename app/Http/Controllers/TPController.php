<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\TP;
use Illuminate\Http\Request;

class TPController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $tp = TP::where('nom_structure', 'LIKE', "%$keyword%")
                ->orWhere('pays', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $tp = TP::latest()->paginate($perPage);
        }

        return view('t-p.index', compact('tp'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('t-p.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'nom_structure' => 'required',
			'nom_structure' => 'mimes:jpeg,jpg,png',
			'nom_structure' => 'max:4000',
			'pays' => 'min:1',
			'pays' => 'max:15'
		]);
        $requestData = $request->all();
                if ($request->hasFile('nom_structure')) {
            $requestData['nom_structure'] = $request->file('nom_structure')
                ->store('uploads', 'public');
        }

        TP::create($requestData);

        return redirect('t-p')->with('flash_message', 'TP added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $tp = TP::findOrFail($id);

        return view('t-p.show', compact('tp'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $tp = TP::findOrFail($id);

        return view('t-p.edit', compact('tp'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'nom_structure' => 'required',
			'nom_structure' => 'mimes:jpeg,jpg,png',
			'nom_structure' => 'max:4000',
			'pays' => 'min:1',
			'pays' => 'max:15'
		]);
        $requestData = $request->all();
                if ($request->hasFile('nom_structure')) {
            $requestData['nom_structure'] = $request->file('nom_structure')
                ->store('uploads', 'public');
        }

        $tp = TP::findOrFail($id);
        $tp->update($requestData);

        return redirect('t-p')->with('flash_message', 'TP updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        TP::destroy($id);

        return redirect('t-p')->with('flash_message', 'TP deleted!');
    }
}
