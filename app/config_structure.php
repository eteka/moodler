<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class config_structure extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'config_structures';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nom', 'logo', 'telephone', 'email', 'description', 'localisation'];

    
}
