<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom','prenom', 'email', 'password','sexe','etat_compte','pseudo','telephone','apropos','poste','photo', 'structure_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'date_inscription'=> 'datetime',
    ];
    public function fullname($v=0){
        if($v>0){
            return $this->prenom ." ".$this->nom;    
        }
        return $this->nom ." ".$this->prenom;
    }

    /**
     * Afficher la structure de l'utilisateur
     */
    public function structure()
    {
        return $this->belongsTo('App\Models\Structure');
    }

    public function photoProfil(){
        $photo=$this->photo;
        //'storage/users/default-img.JPEG'
        if($photo==NULL){
            $photo='storage/moodler/users/default-img.jpg';
        }
        return $photo;
    }
}
